/* -*- indent-tabs-mode: t -*- */

// Copyright (C) 2019-2023 Lawrence Livermore National Security, LLC., Xavier Andrade, Alfredo A. Correa
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#include <systems/ions.hpp>
#include <systems/electrons.hpp>
#include <utils/match.hpp>
#include <ground_state/initial_guess.hpp>
#include <ground_state/calculate.hpp>

#include <input/environment.hpp>

int main(int argc, char ** argv){

	using namespace inq;
	using namespace inq::magnitude;

  int const reps = 10;
  
	input::environment env(argc, argv);

  boost::mpi3::cartesian_communicator<2> cart_comm(boost::mpi3::environment::get_world_instance(), {1, boost::mpi3::fill});

	auto basis_comm = cart_comm.axis(1);

  for(auto ecut = 25.0_Ha; ecut <= 300.0_Ha; ecut += 25.0_Ha){
    
    systems::box box = systems::box::cubic(6.66_b).cutoff_energy(ecut);
    basis::real_space rs(box, basis_comm);
    
    for(int ist = 1; ist <= 256; ist *= 2){
      
      basis::field_set<basis::real_space, complex> phi(rs, ist, cart_comm);
      
      auto iter_start_time = std::chrono::high_resolution_clock::now();
      
      for(int irep = 0; irep < reps; irep++){
        auto fphi = operations::space::to_fourier(phi);
      }
      
      auto new_time = std::chrono::high_resolution_clock::now();
      std::chrono::duration<double> elapsed_seconds = new_time - iter_start_time;
    
      std::cout << ecut.in_atomic_units() << '\t' << ist << '\t' << elapsed_seconds.count()/reps*1000.0 << "\tms" << std::endl;

    }
  }
  
}
