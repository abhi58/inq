# Copyright (C) 2019-2020 Xavier Andrade, Alfredo A. Correa

image: debian:stable-backports

default:
  artifacts:
    expire_in: 2 days
    
variables:
  GIT_SUBMODULE_STRATEGY: recursive
  CXXFLAGS: "-O3 -pipe"
  OMPI_ALLOW_RUN_AS_ROOT: "1"
  OMPI_ALLOW_RUN_AS_ROOT_CONFIRM: "1"
# avoid the annoying messages from openmpi over docker, https://github.com/open-mpi/ompi/issues/4948
  OMPI_MCA_btl_vader_single_copy_mechanism: "none"
  OMPI_MCA_rmaps_base_oversubscribe: "1" 
  CODECOV_TOKEN: "5855e536-b784-400e-b0b2-b1d01c2287a9"

stages:
  - build
  - test
  - clean
  - template

#########################################################################################################################
# CHECK MISSING LIBRARY DETECTION 
#########################################################################################################################

build-noblas:
    stage: build
    script:
      - time apt update -qq 
      - time apt install --no-install-recommends -y -qq pixz git ca-certificates ssh g++ gfortran clang make cmake pkg-config libfftw3-dev libopenmpi-dev openmpi-bin libhdf5-dev libhdf5-openmpi-dev libboost-serialization-dev libboost-filesystem-dev python2
      - if [ -v UPDATE_MULTI ] ; then cd external_libs/multi && git checkout master && git pull && cd - ; fi
      - mpic++ --version
      - mkdir build
      - cd build
      - export CXXFLAGS="-O3 -pipe -Wall -Werror -Wpedantic -pedantic-errors  -Wextra -Wno-cast-function-type -Wno-error=cast-function-type -Wno-unused-parameter -Wno-error=unused-parameter"
      - eval ! ../configure --prefix=$HOME

build-noboost:
    stage: build
    script:
      - time apt update -qq
      - time apt install --no-install-recommends -y -qq pixz git ca-certificates ssh g++ gfortran clang make cmake pkg-config libblas-dev liblapack-dev libfftw3-dev libopenmpi-dev openmpi-bin libhdf5-dev libhdf5-openmpi-dev python2
      - if [ -v UPDATE_MULTI ] ; then cd external_libs/multi && git checkout master && git pull && cd - ; fi
      - mpic++ --version
      - mkdir build
      - cd build
      - export CXXFLAGS="-O3 -pipe -Wall -Werror -Wpedantic -pedantic-errors  -Wextra -Wno-cast-function-type -Wno-error=cast-function-type -Wno-unused-parameter -Wno-error=unused-parameter"
      - eval ! ../configure --prefix=$HOME

build-nofftw:
    stage: build
    script:
      - time apt update -qq
      - time apt install --no-install-recommends -y -qq pixz git ca-certificates ssh g++ gfortran clang make cmake pkg-config libblas-dev liblapack-dev libopenmpi-dev openmpi-bin libhdf5-dev libhdf5-openmpi-dev libboost-serialization-dev libboost-filesystem-dev python2
      - if [ -v UPDATE_MULTI ] ; then cd external_libs/multi && git checkout master && git pull && cd - ; fi
      - mpic++ --version
      - mkdir build
      - cd build
      - export CXXFLAGS="-O3 -pipe -Wall -Werror -Wpedantic -pedantic-errors  -Wextra -Wno-cast-function-type -Wno-error=cast-function-type -Wno-unused-parameter -Wno-error=unused-parameter"
      - eval ! ../configure --prefix=$HOME

build-nolapack:
    stage: build
    script:
      - time apt update -qq 
      - time apt install --no-install-recommends -y -qq pixz git ca-certificates ssh g++ gfortran clang make cmake pkg-config libblas-dev libfftw3-dev libopenmpi-dev openmpi-bin libhdf5-dev libhdf5-openmpi-dev libboost-serialization-dev libboost-filesystem-dev python2
      - if [ -v UPDATE_MULTI ] ; then cd external_libs/multi && git checkout master && git pull && cd - ; fi
      - mpic++ --version
      - mkdir build
      - cd build
      - export CXXFLAGS="-O3 -pipe -Wall -Werror -Wpedantic -pedantic-errors  -Wextra -Wno-cast-function-type -Wno-error=cast-function-type -Wno-unused-parameter -Wno-error=unused-parameter"
      - eval ! ../configure --prefix=$HOME

build-nohdf5:
    stage: build
    script:
      - time apt update -qq 
      - time apt install --no-install-recommends -y -qq pixz git ca-certificates ssh g++ gfortran clang make cmake pkg-config libblas-dev liblapack-dev libfftw3-dev libopenmpi-dev openmpi-bin libboost-serialization-dev libboost-filesystem-dev python2
      - if [ -v UPDATE_MULTI ] ; then cd external_libs/multi && git checkout master && git pull && cd - ; fi
      - mpic++ --version
      - mkdir build
      - cd build
      - export CXXFLAGS="-O3 -pipe -Wall -Werror -Wpedantic -pedantic-errors  -Wextra -Wno-cast-function-type -Wno-error=cast-function-type -Wno-unused-parameter -Wno-error=unused-parameter"
      - eval ! ../configure --prefix=$HOME

#########################################################################################################################
# GCC OMPI COVERAGE
#########################################################################################################################

gcc-ompi-coverage:
    stage: test
    needs: []
    script:
      - time apt update -qq
      - time apt install --no-install-recommends -y -qq pixz curl git ca-certificates ssh g++ gfortran make cmake pkg-config libblas-dev liblapack-dev libfftw3-dev libopenmpi-dev openmpi-bin libboost-serialization-dev libboost-filesystem-dev python2 libhdf5-dev libhdf5-openmpi-dev
      - mkdir build && cd build
      - export CXXFLAGS="-O3 -Wall -Wextra --coverage"      
      - ../configure --disable-debug --prefix=$HOME
      - time make
      - time make install
      - cd ../
      - time tar -Ipixz -cvf gcc-ompi-coverage.tar.xz build/
      - ls -lh gcc-ompi-coverage.tar.xz
    artifacts:
      paths:
      - gcc-ompi-coverage.tar.xz

gcc-ompi-coverage-1p:
    stage: test
    needs: ["gcc-ompi-coverage"]
    script:
      - time apt update -qq
      - time apt install --no-install-recommends -y -qq pixz curl git ca-certificates ssh g++ gfortran make cmake pkg-config libblas-dev liblapack-dev libfftw3-dev libopenmpi-dev openmpi-bin libboost-serialization-dev libboost-filesystem-dev python2 libhdf5-dev libhdf5-openmpi-dev
      - time tar -Ipixz -xvf gcc-ompi-coverage.tar.xz
      - cd build
      - cmake --build . --target install/fast
      - cd src; ctest --output-on-failure --timeout 2000; cd ..
      - cd tests; ctest --output-on-failure --timeout 2000; cd ..
      - cd speed_tests; ctest --output-on-failure --timeout 2000 || echo "Ignoring failed speed test"; cd ..
      - bash <(curl -s https://codecov.io/bash) || echo 'Codecov failed to upload'

gcc-ompi-coverage-4p:
    stage: test
    needs: ["gcc-ompi-coverage"]
    script:
      - time apt update -qq
      - time apt install --no-install-recommends -y -qq pixz curl git ca-certificates ssh g++ gfortran make cmake pkg-config libblas-dev liblapack-dev libfftw3-dev libopenmpi-dev openmpi-bin libboost-serialization-dev libboost-filesystem-dev python2 libhdf5-dev libhdf5-openmpi-dev
      - time tar -Ipixz -xvf gcc-ompi-coverage.tar.xz
      - cd build
      - cmake --build . --target install/fast
      - cd src; INQ_EXEC_ENV="mpirun -np 4" ctest --output-on-failure --timeout 2000; cd ..
      - cd tests; INQ_EXEC_ENV="mpirun -np 4" ctest --output-on-failure --timeout 2000; cd ..
      - cd speed_tests; INQ_EXEC_ENV="mpirun -np 4" ctest --output-on-failure --timeout 2000 || echo "Ignoring failed speed test"; cd ..
      - bash <(curl -s https://codecov.io/bash) || echo 'Codecov failed to upload'      

#############################################################################################
# GCC-MPICH
#############################################################################################

gcc-mpich:
    stage: build
    script:
      - apt update -qq && apt install --no-install-recommends -y -qq pixz git ca-certificates ssh g++ gfortran make cmake pkg-config libblas-dev liblapack-dev libfftw3-dev libboost-serialization-dev libboost-filesystem-dev python2 valgrind libmpich-dev mpich libhdf5-dev libhdf5-mpich-dev
      - if [ -v UPDATE_MULTI ] ; then cd external_libs/multi && git checkout master && git pull && cd - ; fi
      - c++ --version
      - mpichversion
      - mkdir build && cd build
      - CXXFLAGS="-ffast-math" ../configure --prefix=$HOME --disable-debug
      - time make
      - time make install
      - $HOME/bin/inc++ ../tests/silicon.cpp -o tests/silicon2
      - cd ../
      - time tar -Ipixz -cvf gcc-mpich-build.tar.xz build/
    artifacts:
      paths:
      - gcc-mpich-build.tar.xz

gcc-mpich-valgrind-utests-1p:
    stage: test
    needs: ["gcc-mpich"]
    script:
      - time apt update -qq
      - time apt install --no-install-recommends -y -qq pixz git ca-certificates ssh g++ gfortran make cmake pkg-config libblas-dev liblapack-dev libfftw3-dev libboost-serialization-dev libboost-filesystem-dev python2 valgrind libmpich-dev mpich libhdf5-dev libhdf5-mpich-dev
      - time tar -Ipixz -xvf gcc-mpich-build.tar.xz
      - cd build
      - cmake --build . --target install/fast
      - export VALGRIND="valgrind --leak-check=full --track-origins=yes --show-leak-kinds=all --error-exitcode=1 --gen-suppressions=all"
      - ulimit -n ; ulimit -n 1024  # neededed by valgrind in docker running in Fedora
      - cd src; INQ_EXEC_ENV="$VALGRIND" ctest --output-on-failure --timeout 7200; cd ..

gcc-mpich-valgrind-itests-1p:
    stage: test
    needs: ["gcc-mpich"]
    script:
      - time apt update -qq
      - time apt install --no-install-recommends -y -qq pixz git ca-certificates ssh g++ gfortran make cmake pkg-config libblas-dev liblapack-dev libfftw3-dev libboost-serialization-dev libboost-filesystem-dev python2 valgrind libmpich-dev mpich libhdf5-dev libhdf5-mpich-dev
      - time tar -Ipixz -xvf gcc-mpich-build.tar.xz
      - cd build
      - cmake --build . --target install/fast
      - ulimit -n ; ulimit -n 1024  # neededed by valgrind in docker running in Fedora
      - export VALGRIND="valgrind --leak-check=full --track-origins=yes --show-leak-kinds=all --error-exitcode=1 --gen-suppressions=all"
      - time $VALGRIND tests/h2o_ground_state
      - time $VALGRIND tests/h2o_real_time
      

gcc-mpich-1p:
    stage: test
    needs: ["gcc-mpich"]
    script:
      - apt update -qq && apt install --no-install-recommends -y -qq pixz git ca-certificates ssh g++ gfortran make cmake pkg-config libblas-dev liblapack-dev libfftw3-dev libboost-serialization-dev libboost-filesystem-dev python2 valgrind libmpich-dev mpich libhdf5-dev libhdf5-mpich-dev
      - time tar -Ipixz -xvf gcc-mpich-build.tar.xz
      - cd build
      - cmake --build . --target install/fast
      - ctest --output-on-failure --timeout 360
      - $HOME/bin/inc++ ../tests/silicon.cpp -o tests/silicon2
      - tests/silicon2

gcc-mpich-2p:
    stage: test
    needs: ["gcc-mpich"]
    script:
      - time apt update -qq
      - time apt install --no-install-recommends -y -qq pixz git ca-certificates ssh g++ gfortran make cmake pkg-config libblas-dev liblapack-dev libfftw3-dev libboost-serialization-dev libboost-filesystem-dev python2 valgrind libmpich-dev mpich libhdf5-dev libhdf5-mpich-dev
      - time tar -Ipixz -xvf gcc-mpich-build.tar.xz
      - cd build
      - cmake --build . --target install/fast
      - INQ_EXEC_ENV="mpirun -np 2" ctest --output-on-failure --timeout 3600

gcc-mpich-3p:
    stage: test
    needs: ["gcc-mpich"]
    script:
      - time apt update -qq
      - time apt install --no-install-recommends -y -qq pixz git ca-certificates ssh g++ gfortran make cmake pkg-config libblas-dev liblapack-dev libfftw3-dev libboost-serialization-dev libboost-filesystem-dev python2 valgrind libmpich-dev mpich libhdf5-dev libhdf5-mpich-dev
      - time tar -Ipixz -xvf gcc-mpich-build.tar.xz
      - cd build
      - cmake --build . --target install/fast
      - INQ_EXEC_ENV="mpirun -np 3" ctest --output-on-failure --timeout 3600

gcc-mpich-4p:
    stage: test
    needs: ["gcc-mpich"]
    script:
      - time apt update -qq
      - time apt install --no-install-recommends -y -qq pixz git ca-certificates ssh g++ gfortran make cmake pkg-config libblas-dev liblapack-dev libfftw3-dev libboost-serialization-dev libboost-filesystem-dev python2 valgrind libmpich-dev mpich libhdf5-dev libhdf5-mpich-dev
      - time tar -Ipixz -xvf gcc-mpich-build.tar.xz
      - cd build
      - cmake --build . --target install/fast
      - INQ_EXEC_ENV="mpirun -np 4" ctest --output-on-failure --timeout 3600

gcc-mpich-5p:
    stage: test
    needs: ["gcc-mpich"]
    script:
      - time apt update -qq
      - time apt install --no-install-recommends -y -qq pixz git ca-certificates ssh g++ gfortran make cmake pkg-config libblas-dev liblapack-dev libfftw3-dev libboost-serialization-dev libboost-filesystem-dev python2 valgrind libmpich-dev mpich libhdf5-dev libhdf5-mpich-dev
      - time tar -Ipixz -xvf gcc-mpich-build.tar.xz
      - cd build
      - cmake --build . --target install/fast
      - cd tests; INQ_EXEC_ENV="mpirun -np 4" ctest --output-on-failure --timeout 3600; cd ..

#########################################################################################################################
# GCC-OMPI
#########################################################################################################################

gcc-ompi:
    stage: test
    needs: []
    script:
      - time apt update -qq
      - time apt install --no-install-recommends -y -qq pixz curl git ca-certificates ssh g++ gfortran make cmake pkg-config libblas-dev liblapack-dev libfftw3-dev libopenmpi-dev openmpi-bin libhdf5-dev libhdf5-openmpi-dev libboost-serialization-dev libboost-filesystem-dev python2
      - if [ -v UPDATE_MULTI ] ; then cd external_libs/multi && git checkout master && git pull && cd - ; fi
      - mpic++ --version
      - mkdir build && cd build
      - ../configure --prefix=$HOME --disable-debug || cat config.log
      - time make
      - time make install
      - ctest --output-on-failure --timeout 1000
      - cd src; INQ_EXEC_ENV="mpirun --allow-run-as-root -np 2" ctest --output-on-failure --timeout 1000; cd ..
      - cd src; INQ_EXEC_ENV="mpirun --allow-run-as-root -np 3" ctest --output-on-failure --timeout 1000; cd ..
      - cd src; INQ_EXEC_ENV="mpirun --allow-run-as-root -np 4" ctest --output-on-failure --timeout 1000; cd ..
      - time mpirun --allow-run-as-root -np 2 tests/silicon
      - time mpirun --allow-run-as-root -np 3 tests/silicon
      - time mpirun --allow-run-as-root -np 5 tests/silicon
      - time mpirun --allow-run-as-root -np 2 tests/nitrogen
      - time mpirun --allow-run-as-root -np 3 tests/nitrogen
      - time mpirun --allow-run-as-root -np 5 tests/nitrogen
      - time mpirun --allow-run-as-root -np 2 tests/al4h1
      - time mpirun --allow-run-as-root -np 3 tests/al4h1
      - time mpirun --allow-run-as-root -np 5 tests/al4h1
      - time mpirun --allow-run-as-root -np 2 tests/al4h1_td
      - time mpirun --allow-run-as-root -np 3 tests/al4h1_td
      - time mpirun --allow-run-as-root -np 5 tests/al4h1_td
      - $HOME/bin/inc++ ../tests/silicon.cpp -o tests/silicon2
      - tests/silicon2

#########################################################################################################################
# GCC-OMPI-FAST
#########################################################################################################################
      
gcc-ompi-fast:
    stage: test
    needs: []
    script:
      - time apt update -qq
      - time apt install --no-install-recommends -y -qq pixz git ca-certificates ssh g++ gfortran  make cmake pkg-config libblas-dev liblapack-dev libfftw3-dev libopenmpi-dev openmpi-bin libhdf5-dev libhdf5-openmpi-dev libboost-serialization-dev libboost-filesystem-dev python2
      - if [ -v UPDATE_MULTI ] ; then cd external_libs/multi && git checkout master && git pull && cd - ; fi
      - mpic++ --version
      - mkdir build && cd build
      - export CXXFLAGS="-Ofast -ffast-math -DNDEBUG -pipe -Wall -Werror -Wpedantic -pedantic-errors -Wextra -Wno-narrowing -Wno-cast-function-type -Wno-unknown-pragmas -Wno-error=cast-function-type -Wno-unused-parameter -Wno-error=unused-parameter -Wno-error=deprecated-declarations"
      - ../configure --prefix=$HOME  || cat config.log
      - time make
      - time make install
      - ctest --output-on-failure --timeout 360
      - cd src; INQ_EXEC_ENV="mpirun --allow-run-as-root -np 2" ctest --output-on-failure --timeout 360; cd ..
      - cd src; INQ_EXEC_ENV="mpirun --allow-run-as-root -np 3" ctest --output-on-failure --timeout 360; cd ..
      - cd src; INQ_EXEC_ENV="mpirun --allow-run-as-root -np 4" ctest --output-on-failure --timeout 360; cd ..
      - time mpirun --allow-run-as-root -np 2 tests/silicon
      - time mpirun --allow-run-as-root -np 3 tests/silicon
      - time mpirun --allow-run-as-root -np 5 tests/silicon
      - time mpirun --allow-run-as-root -np 2 tests/nitrogen
      - time mpirun --allow-run-as-root -np 3 tests/nitrogen
      - time mpirun --allow-run-as-root -np 5 tests/nitrogen
      - time mpirun --allow-run-as-root -np 2 tests/al4h1
      - time mpirun --allow-run-as-root -np 3 tests/al4h1
      - time mpirun --allow-run-as-root -np 5 tests/al4h1
      - time mpirun --allow-run-as-root -np 2 tests/al4h1_td
      - time mpirun --allow-run-as-root -np 3 tests/al4h1_td
      - time mpirun --allow-run-as-root -np 5 tests/al4h1_td
      - $HOME/bin/inc++ ../tests/silicon.cpp -o tests/silicon2
      - tests/silicon2

#########################################################################################################################
# CLANG-OMPI
#########################################################################################################################

clang-ompi:
    stage: build
    script:
      - time apt update -qq
      - time apt install --no-install-recommends -y -qq pixz git ca-certificates ssh g++ gfortran clang-9 make cmake pkg-config libblas-dev liblapack-dev libfftw3-dev libopenmpi-dev openmpi-bin libhdf5-dev libhdf5-openmpi-dev libboost-serialization-dev libboost-filesystem-dev python2
      - if [ -v UPDATE_MULTI ] ; then cd external_libs/multi && git checkout master && git pull && cd - ; fi
      - clang++-9 --version
      - mkdir build
      - cd build
      - CXXFLAGS="-O3 -pipe -Wall -Werror"
      - CXX=clang++-9 CC=clang-9 ../configure --prefix=$HOME
      - ls -l
      - time make
      - time make install
      - $HOME/bin/inc++ ../tests/silicon.cpp -o tests/silicon2
      - cd ../
      - time tar -Ipixz -cvf clang-ompi-build.tar.xz build/
    artifacts:
      paths:
      - clang-ompi-build.tar.xz

clang-ompi-1p:
    stage: test
    needs: ["clang-ompi"]    
    script:
      - time apt update -qq
      - time apt install --no-install-recommends -y -qq pixz git ca-certificates ssh g++ gfortran clang-9 make cmake pkg-config libblas-dev liblapack-dev libfftw3-dev libopenmpi-dev openmpi-bin libhdf5-dev libhdf5-openmpi-dev libboost-serialization-dev libboost-filesystem-dev python2
      - time tar -Ipixz -xvf clang-ompi-build.tar.xz
      - cd build
      - cmake --build . --target install/fast
      - ctest --output-on-failure --timeout 360
      - tests/silicon2

clang-ompi-2p:
    stage: test
    needs: ["clang-ompi"]
    script:
      - time apt update -qq
      - time apt install --no-install-recommends -y -qq pixz git ca-certificates ssh g++ gfortran clang-9 make cmake pkg-config libblas-dev liblapack-dev libfftw3-dev libopenmpi-dev openmpi-bin libhdf5-dev libhdf5-openmpi-dev libboost-serialization-dev libboost-filesystem-dev python2
      - time tar -Ipixz -xvf clang-ompi-build.tar.xz
      - cd build
      - cmake --build . --target install/fast
      - INQ_EXEC_ENV="mpirun --allow-run-as-root -np 2" ctest --output-on-failure --timeout 3600
      
clang-ompi-3p:
    stage: test
    needs: ["clang-ompi"]
    script:
      - time apt update -qq
      - time apt install --no-install-recommends -y -qq pixz git ca-certificates ssh g++ gfortran clang-9 make cmake pkg-config libblas-dev liblapack-dev libfftw3-dev libopenmpi-dev openmpi-bin libhdf5-dev libhdf5-openmpi-dev libboost-serialization-dev libboost-filesystem-dev python2
      - time tar -Ipixz -xvf clang-ompi-build.tar.xz
      - cd build
      - cmake --build . --target install/fast
      - INQ_EXEC_ENV="mpirun --allow-run-as-root -np 3" ctest --output-on-failure --timeout 3600

clang-ompi-4p:
    stage: test
    needs: ["clang-ompi"]
    script:
      - time apt update -qq
      - time apt install --no-install-recommends -y -qq pixz git ca-certificates ssh g++ gfortran clang-9 make cmake pkg-config libblas-dev liblapack-dev libfftw3-dev libopenmpi-dev openmpi-bin libhdf5-dev libhdf5-openmpi-dev libboost-serialization-dev libboost-filesystem-dev python2
      - time tar -Ipixz -xvf clang-ompi-build.tar.xz
      - cd build
      - cmake --build . --target install/fast
      - INQ_EXEC_ENV="mpirun --allow-run-as-root -np 4" ctest --output-on-failure --timeout 3600

clang-ompi-5p:
    stage: test
    needs: ["clang-ompi"]
    script:
      - time apt update -qq
      - time apt install --no-install-recommends -y -qq pixz git ca-certificates ssh g++ gfortran clang-9 make cmake pkg-config libblas-dev liblapack-dev libfftw3-dev libopenmpi-dev openmpi-bin libhdf5-dev libhdf5-openmpi-dev libboost-serialization-dev libboost-filesystem-dev python2
      - time tar -Ipixz -xvf clang-ompi-build.tar.xz
      - cd build
      - cmake --build . --target install/fast
      - cd tests; INQ_EXEC_ENV="mpirun --allow-run-as-root -np 5" ctest --output-on-failure --timeout 3600; cd ..

#########################################################################################################################
# CLANG-OMPI-FAST
#########################################################################################################################

clang-ompi-fast:
    stage: build
    script:
      - time apt update -qq
      - time apt install --no-install-recommends -y -qq pixz git ca-certificates ssh g++ gfortran clang-9 make cmake pkg-config libblas-dev liblapack-dev libfftw3-dev libopenmpi-dev openmpi-bin libhdf5-dev libhdf5-openmpi-dev libboost-serialization-dev libboost-filesystem-dev python2
      - if [ -v UPDATE_MULTI ] ; then cd external_libs/multi && git checkout master && git pull && cd - ; fi
      - clang++-9 --version
      - mkdir build && cd build
      - CXX=clang++-9 ../configure --prefix=$HOME --disable-debug
      - time make -j4
      - time make -j4 install
      - $HOME/bin/inc++ ../tests/silicon.cpp -o tests/silicon2
      - cd ../
      - time tar -Ipixz -cvf clang-ompi-fast-build.tar.xz build/
    artifacts:
      paths:
      - clang-ompi-fast-build.tar.xz

clang-ompi-fast-1p:
    stage: test
    needs: ["clang-ompi-fast"]
    script:
      - export INQ_COMM="point"      
      - time apt update -qq
      - time apt install --no-install-recommends -y -qq pixz git ca-certificates ssh g++ gfortran clang-9 make cmake pkg-config libblas-dev liblapack-dev libfftw3-dev libopenmpi-dev openmpi-bin libhdf5-dev libhdf5-openmpi-dev libboost-serialization-dev libboost-filesystem-dev python2
      - time tar -Ipixz -xvf clang-ompi-fast-build.tar.xz
      - cd build
      - cmake --build . --target install/fast      
      - ctest --output-on-failure --timeout 360
      - tests/silicon2

clang-ompi-fast-2p:
    stage: test
    needs: ["clang-ompi-fast"]    
    script:
      - export INQ_COMM="point"      
      - time apt update -qq
      - time apt install --no-install-recommends -y -qq pixz git ca-certificates ssh g++ gfortran clang-9 make cmake pkg-config libblas-dev liblapack-dev libfftw3-dev libopenmpi-dev openmpi-bin libhdf5-dev libhdf5-openmpi-dev libboost-serialization-dev libboost-filesystem-dev python2
      - time tar -Ipixz -xvf clang-ompi-fast-build.tar.xz
      - cd build
      - cmake --build . --target install/fast
      - INQ_EXEC_ENV="mpirun --allow-run-as-root -np 2" ctest --output-on-failure --timeout 3600

clang-ompi-fast-3p:
    stage: test
    needs: ["clang-ompi-fast"]    
    script:
      - export INQ_COMM="point"      
      - time apt update -qq
      - time apt install --no-install-recommends -y -qq pixz git ca-certificates ssh g++ gfortran clang-9 make cmake pkg-config libblas-dev liblapack-dev libfftw3-dev libopenmpi-dev openmpi-bin libhdf5-dev libhdf5-openmpi-dev libboost-serialization-dev libboost-filesystem-dev python2
      - time tar -Ipixz -xvf clang-ompi-fast-build.tar.xz
      - cd build
      - cmake --build . --target install/fast
      - INQ_EXEC_ENV="mpirun --allow-run-as-root -np 3" ctest --output-on-failure --timeout 3600

clang-ompi-fast-4p:
    stage: test
    needs: ["clang-ompi-fast"]    
    script:
      - export INQ_COMM="point"      
      - time apt update -qq
      - time apt install --no-install-recommends -y -qq pixz git ca-certificates ssh g++ gfortran clang-9 make cmake pkg-config libblas-dev liblapack-dev libfftw3-dev libopenmpi-dev openmpi-bin libhdf5-dev libhdf5-openmpi-dev libboost-serialization-dev libboost-filesystem-dev python2
      - time tar -Ipixz -xvf clang-ompi-fast-build.tar.xz
      - cd build
      - cmake --build . --target install/fast
      - INQ_EXEC_ENV="mpirun --allow-run-as-root -np 4" ctest --output-on-failure --timeout 3600
      
clang-ompi-fast-5p:
    stage: test
    needs: ["clang-ompi-fast"]    
    script:
      - export INQ_COMM="point"      
      - time apt update -qq
      - time apt install --no-install-recommends -y -qq pixz git ca-certificates ssh g++ gfortran clang-9 make cmake pkg-config libblas-dev liblapack-dev libfftw3-dev libopenmpi-dev openmpi-bin libhdf5-dev libhdf5-openmpi-dev libboost-serialization-dev libboost-filesystem-dev python2
      - time tar -Ipixz -xvf clang-ompi-fast-build.tar.xz
      - cd build
      - cmake --build . --target install/fast
      - cd tests; INQ_EXEC_ENV="mpirun --allow-run-as-root -np 5" ctest --output-on-failure --timeout 3600; cd ..

#########################################################################################################################
# INTEL MPICH
#########################################################################################################################

intel-mpich:
    stage: build
    script:
      - time apt update -qq
      - time apt install --no-install-recommends -y -qq pixz curl git ca-certificates ssh g++ gfortran clang-9 make cmake pkg-config libblas-dev liblapack-dev libfftw3-dev libmpich-dev mpich libhdf5-dev libhdf5-mpich-dev libboost-serialization-dev libboost-filesystem-dev python2
      - curl --output webimage.sh --url https://registrationcenter-download.intel.com/akdlm/irc_nas/18479/l_HPCKit_p_2022.1.2.117_offline.sh --retry 5 --retry-delay 5
      - chmod +x webimage.sh
      - ./webimage.sh -x -f webimage_extracted --log extract.log
      - rm -rf webimage.sh
      - ls -1 webimage_extracted/
      - WEBIMAGE_NAME=$(ls -1 webimage_extracted/)
      - webimage_extracted/"$WEBIMAGE_NAME"/bootstrapper -s --action install --components=intel.oneapi.lin.dpcpp-cpp-compiler-pro:intel.oneapi.lin.mpi.devel --eula=accept --log-dir=.
      - rm -rf webimage_extracted
      - . /opt/intel/oneapi/setvars.sh
      - mkdir build
      - cd build
      - export CC=icc
      - export CXX=icpc
      - export FC=gfortran
      - time ../configure --prefix=$HOME --disable-debug --pass-thru -DMPI_EXECUTABLE_SUFFIX=.mpich
      - make
      - make install
      - $HOME/bin/inc++ ../tests/silicon.cpp -o tests/silicon2
      - ldd tests/nitrogen
      - cd ../
      - time tar -Ipixz -cvf intel-mpich-build.tar.xz build/
    artifacts:
      paths:
      - intel-mpich-build.tar.xz

intel-mpich-1p:
    stage: test
    needs: ["intel-mpich"]
    script:
      - time apt update -qq
      - time apt install --no-install-recommends -y -qq pixz curl git ca-certificates ssh g++ gfortran clang-9 make cmake pkg-config libblas-dev liblapack-dev libfftw3-dev libmpich-dev mpich libhdf5-dev libhdf5-mpich-dev libboost-serialization-dev libboost-filesystem-dev python2
      - time tar -Ipixz -xvf intel-mpich-build.tar.xz
      - cd build
      - cmake --build . --target install/fast
      - cd src/; ctest --output-on-failure --timeout 720; cd ../
      - cd tests/; ctest --output-on-failure --timeout 720; cd ../
      - cd speed_tests/; ctest --output-on-failure --timeout 720; cd ../
      - tests/silicon2

intel-mpich-2p:
    stage: test
    needs: ["intel-mpich"]
    script:
      - time apt update -qq
      - time apt install --no-install-recommends -y -qq pixz curl git ca-certificates ssh g++ gfortran clang-9 make cmake pkg-config libblas-dev liblapack-dev libfftw3-dev libmpich-dev mpich libhdf5-dev libhdf5-mpich-dev libboost-serialization-dev libboost-filesystem-dev python2
      - time tar -Ipixz -xvf intel-mpich-build.tar.xz
      - cd build
      - cmake --build . --target install/fast
      - cd src/; INQ_EXEC_ENV="mpirun.mpich -np 2" ctest --output-on-failure --timeout 7200; cd ../
      - cd tests/; INQ_EXEC_ENV="mpirun.mpich -np 2" ctest --output-on-failure --timeout 7200; cd ../
     
intel-mpich-3p:
    stage: test
    needs: ["intel-mpich"]
    script:
      - time apt update -qq
      - time apt install --no-install-recommends -y -qq pixz curl git ca-certificates ssh g++ gfortran clang-9 make cmake pkg-config libblas-dev liblapack-dev libfftw3-dev libmpich-dev mpich libhdf5-dev libhdf5-mpich-dev libboost-serialization-dev libboost-filesystem-dev python2
      - time tar -Ipixz -xvf intel-mpich-build.tar.xz
      - cd build
      - cmake --build . --target install/fast
      - cd src/; INQ_EXEC_ENV="mpirun.mpich -np 3" ctest --output-on-failure --timeout 7200; cd ../
      - cd tests/; INQ_EXEC_ENV="mpirun.mpich -np 3" ctest --output-on-failure --timeout 7200; cd ../

#########################################################################################################################
# NVIDIA MPICH FAST
#########################################################################################################################

nvcc-mpich-fast:
    stage: build
    needs: []
    tags:
      - cuda_gpu
    script:
      - export PREFIX=`mktemp -d /tmp/inq-build-XXXXXXXXXXXXXX`
      - export SRCDIR=`pwd`
      - if [ -v UPDATE_MULTI ] ; then cd external_libs/multi && git checkout master && git pull && cd - ; fi
      - export PATH=/usr/local/cuda/bin:$PATH
      - cmake --version
      - nvcc -V
      - mkdir -p $PREFIX/build
      - cd $PREFIX/build
      - $SRCDIR/configure --prefix=$PREFIX/install --enable-cuda --disable-debug --with-cuda-prefix=/usr/local/cuda --pass-thru -DCMAKE_CUDA_ARCHITECTURES=70 -DMPI_EXECUTABLE_SUFFIX=.mpich
      - time make -j16
      - time make -j16 install
      - cd $SRCDIR
      - echo $PREFIX > nvcc-mpich-fast-path
    artifacts:
      paths:
      - nvcc-mpich-fast-path

nvcc-mpich-fast-1p:
    stage: test
    needs: ["nvcc-mpich-fast"]
    tags:
      - cuda_gpu
    script:
      - PREFIX=`cat nvcc-mpich-fast-path`
      - INQ_EXEC_ENV="mpirun.mpich -np 1" make test -C $PREFIX/build/src ARGS="--output-on-failure --timeout 600 -j8"
      - INQ_EXEC_ENV="mpirun.mpich -np 1" make test -C $PREFIX/build/tests ARGS="--output-on-failure --timeout 1800 -j8"
      - INQ_EXEC_ENV="mpirun.mpich -np 1" make test -C $PREFIX/build/speed_tests ARGS="--output-on-failure --timeout 1800 -j8"
      
nvcc-mpich-fast-2p:
    stage: test
    needs: ["nvcc-mpich-fast"]
    tags:
      - cuda_gpu
    script:
      - PREFIX=`cat nvcc-mpich-fast-path`
      - INQ_EXEC_ENV="mpirun.mpich -np 2" make test -C $PREFIX/build/src ARGS="--output-on-failure --timeout 600 -j4"
      - INQ_EXEC_ENV="mpirun.mpich -np 2" make test -C $PREFIX/build/tests ARGS="--output-on-failure --timeout 1800 -j4"

nvcc-mpich-fast-3p:
    stage: test
    needs: ["nvcc-mpich-fast"]
    tags:
      - cuda_gpu
    script:
      - PREFIX=`cat nvcc-mpich-fast-path`
      - INQ_EXEC_ENV="mpirun.mpich -np 3" make test -C $PREFIX/build/src ARGS="--output-on-failure --timeout 600 -j2"
      - INQ_EXEC_ENV="mpirun.mpich -np 3" make test -C $PREFIX/build/tests ARGS="--output-on-failure --timeout 1800 -j2"

nvcc-mpich-fast-4p:
    stage: test
    needs: ["nvcc-mpich-fast"]
    tags:
      - cuda_gpu
    script:
      - PREFIX=`cat nvcc-mpich-fast-path`
      - INQ_EXEC_ENV="mpirun.mpich -np 4" make test -C $PREFIX/build/src ARGS="--output-on-failure --timeout 600 -j2"
      - INQ_EXEC_ENV="mpirun.mpich -np 4" make test -C $PREFIX/build/tests ARGS="--output-on-failure --timeout 1800 -j2"

nvcc-mpich-fast-5p:
    stage: test
    needs: ["nvcc-mpich-fast"]
    tags:
      - cuda_gpu
    script:
      - PREFIX=`cat nvcc-mpich-fast-path`
      - INQ_EXEC_ENV="mpirun.mpich -np 5" make test -C $PREFIX/build/tests ARGS="--output-on-failure --timeout 1800 -j1"      

nvcc-mpich-fast-clean:
    stage: clean
    needs:
     - nvcc-mpich-fast
     - nvcc-mpich-fast-1p
     - nvcc-mpich-fast-2p
     - nvcc-mpich-fast-3p
     - nvcc-mpich-fast-4p
     - nvcc-mpich-fast-5p
    tags:
      - cuda_gpu
    script:
      - PREFIX=`cat nvcc-mpich-fast-path`
      - rm -vfr $PREFIX

#########################################################################################################################
# NVIDIA MPICH DEBUG
#########################################################################################################################

nvcc-mpich-debug:
    stage: build
    needs: []
    tags:
      - cuda_gpu
    script:
      - export PREFIX=`mktemp -d /tmp/inq-build-XXXXXXXXXXXXXX`
      - export SRCDIR=`pwd`
      - if [ -v UPDATE_MULTI ] ; then cd external_libs/multi && git checkout master && git pull && cd - ; fi
      - export PATH=/usr/local/cuda/bin:$PATH
      - cmake --version
      - nvcc -V
      - mkdir -p $PREFIX/build
      - cd $PREFIX/build
      - $SRCDIR/configure --prefix=$PREFIX/install --enable-cuda --with-cuda-prefix=/usr/local/cuda --pass-thru -DCMAKE_CUDA_ARCHITECTURES=70 -DMPI_EXECUTABLE_SUFFIX=.mpich
      - time make -j16
      - time make -j16 install
      - cd $SRCDIR
      - echo $PREFIX > nvcc-mpich-debug-path
    artifacts:
      paths:
      - nvcc-mpich-debug-path

nvcc-mpich-debug-1p:
    stage: test
    needs: ["nvcc-mpich-debug"]
    tags:
      - cuda_gpu
    script:
      - export INQ_COMM="point"
      - PREFIX=`cat nvcc-mpich-debug-path`
      - INQ_EXEC_ENV="mpirun.mpich -np 1" make test -C $PREFIX/build/src ARGS="--output-on-failure --timeout 600 -j8"
      - INQ_EXEC_ENV="mpirun.mpich -np 1" make test -C $PREFIX/build/tests ARGS="--output-on-failure --timeout 1800 -j8"
      - INQ_EXEC_ENV="mpirun.mpich -np 1" make test -C $PREFIX/build/speed_tests ARGS="--output-on-failure --timeout 1800 -j8"

nvcc-mpich-debug-2p:
    stage: test
    needs: ["nvcc-mpich-debug"]
    tags:
      - cuda_gpu
    script:
      - export INQ_COMM="point"      
      - PREFIX=`cat nvcc-mpich-debug-path`
      - INQ_EXEC_ENV="mpirun.mpich -np 2" make test -C $PREFIX/build/src ARGS="--output-on-failure --timeout 600 -j4"
      - INQ_EXEC_ENV="mpirun.mpich -np 2" make test -C $PREFIX/build/tests ARGS="--output-on-failure --timeout 1800 -j4"      

nvcc-mpich-debug-3p:
    stage: test
    needs: ["nvcc-mpich-debug"]
    tags:
      - cuda_gpu
    script:
      - export INQ_COMM="point"      
      - PREFIX=`cat nvcc-mpich-debug-path`
      - INQ_EXEC_ENV="mpirun.mpich -np 3" make test -C $PREFIX/build/src ARGS="--output-on-failure --timeout 600 -j2"
      - INQ_EXEC_ENV="mpirun.mpich -np 3" make test -C $PREFIX/build/tests ARGS="--output-on-failure --timeout 1800 -j2"

nvcc-mpich-debug-4p:
    stage: test
    needs: ["nvcc-mpich-debug"]
    tags:
      - cuda_gpu
    script:
      - export INQ_COMM="point"      
      - PREFIX=`cat nvcc-mpich-debug-path`
      - INQ_EXEC_ENV="mpirun.mpich -np 4" make test -C $PREFIX/build/src ARGS="--output-on-failure --timeout 600 -j2"
      - INQ_EXEC_ENV="mpirun.mpich -np 4" make test -C $PREFIX/build/tests ARGS="--output-on-failure --timeout 1800 -j2"      

nvcc-mpich-debug-5p:
    stage: test
    needs: ["nvcc-mpich-debug"]
    tags:
      - cuda_gpu
    script:
      - export INQ_COMM="point"      
      - PREFIX=`cat nvcc-mpich-debug-path`
      - INQ_EXEC_ENV="mpirun.mpich -np 5" make test -C $PREFIX/build/tests ARGS="--output-on-failure --timeout 1800 -j1"

nvcc-mpich-debug-clean:
    stage: clean
    needs:
     - nvcc-mpich-debug
     - nvcc-mpich-debug-1p
     - nvcc-mpich-debug-2p
     - nvcc-mpich-debug-3p
     - nvcc-mpich-debug-4p
     - nvcc-mpich-debug-5p
    tags:
      - cuda_gpu
    script:
      - PREFIX=`cat nvcc-mpich-debug-path`
      - rm -vfr $PREFIX

#######################################################################################################################

gcc-mpich-template:
    variables:
      GIT_STRATEGY: none
    stage: template
    needs: []
    script:
      - apt update -qq && apt install --no-install-recommends -y -qq pixz git ca-certificates ssh g++ gfortran make cmake pkg-config libblas-dev liblapack-dev libfftw3-dev libboost-serialization-dev libboost-filesystem-dev python2 libmpich-dev mpich libhdf5-dev libhdf5-mpich-dev
      - mkdir inq_template_dir
      - cd inq_template_dir
      - git clone https://gitlab.com/npneq/inq_template.git
      - cd inq_template
      - mkdir build && cd build
      - time cmake ../ -DCMAKE_INSTALL_PREFIX=`pwd`/../install/ -DCMAKE_BUILD_TYPE=Release
      - time make -j2
      - time make -j2 install
      - cd example
      - ls
      - make nitrogen
      - mpirun -np 2 ./nitrogen

#######################################################################################################################

nvcc-mpich-template:
    variables:
      GIT_STRATEGY: none
    stage: template
    needs: []
    tags:
      - cuda_gpu
    script:
      - mkdir inq_template_dir
      - cd inq_template_dir
      - git clone https://gitlab.com/npneq/inq_template.git
      - cd inq_template
      - mkdir build && cd build
      - time cmake ../ -DCMAKE_INSTALL_PREFIX=`pwd`/../install/ -DCMAKE_BUILD_TYPE=Release -DENABLE_CUDA=ON -DCMAKE_CUDA_ARCHITECTURES=70
      - time make -j2
      - time make -j2 install
      - cd example
      - ls
      - make nitrogen
      - mpirun -np 2 ./nitrogen

#######################################################################################################################

clang-ompi-template:
    variables:
      GIT_STRATEGY: none
    stage: template
    needs: []
    script:
      - time apt update -qq
      - time apt install --no-install-recommends -y -qq git ca-certificates ssh gfortran clang make cmake pkg-config libblas-dev liblapack-dev libfftw3-dev libopenmpi-dev openmpi-bin libhdf5-dev libhdf5-openmpi-dev libboost-serialization-dev libboost-filesystem-dev python2 wget
      - mkdir inq_template_dir
      - cd inq_template_dir
      - wget https://gitlab.com/npneq/inq_template/-/archive/main/inq_template-main.tar.gz
      - tar -xvzf inq_template-main.tar.gz
      - cd inq_template-main
      - mkdir build && cd build
      - export CXXFLAGS="-O3 -pipe -Wall -Werror"
      - export CXX=clang++
      - export CC=clang
      - time cmake ../ -DCMAKE_INSTALL_PREFIX=`pwd`/../install/ -DCMAKE_BUILD_TYPE=Release -DCMAKE_CUDA_ARCHITECTURES=70
      - time make -j2
      - time make -j2 install
      - cd example
      - ls
      - make nitrogen
      - mpirun -np 2 ./nitrogen

# vim: set sw=4 ts=4 sts=4 tw=80 et nowrap

